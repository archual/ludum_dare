var InfiniteScroller = InfiniteScroller || {};

//loading the game title
InfiniteScroller.GameTitle = function(){};

InfiniteScroller.GameTitle.prototype = {
	
  	create: function(){
  	 	//  Our tiled scrolling background
    	let land = this.add.tileSprite(0, 0, 960, 540, 'startScreen');
    	land.fixedToCamera = true;
    	
		let playButton = this.add.button(this.game.world.centerX, this.game.world.centerY, "go", this.playTheGame, this);
		playButton.anchor.setTo(0.5,0.5);
	},
	
	playTheGame: function(){
	    this.state.start('Game');
	}
}