var InfiniteScroller = InfiniteScroller || {};

//loading the game title
InfiniteScroller.GameOver = function(){};

InfiniteScroller.GameOver.prototype = {
	init: function(score){
	},
  	create: function(){
  		var gameOverTitle = this.game.add.sprite(160,160,"game_over_primer");
		gameOverTitle.anchor.setTo(0.5,0.5);
		var playButton = this.game.add.button(160,320,"play",this.playTheGame,this);
		playButton.anchor.setTo(0.5,0.5);
	},
	playTheGame: function(){
		this.game.state.start("Game");
	}
}