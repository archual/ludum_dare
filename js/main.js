var InfiniteScroller = InfiniteScroller || {};

InfiniteScroller.game = new Phaser.Game(960, 540, Phaser.CANVAS, '');

InfiniteScroller.game.state.add('Boot', InfiniteScroller.Boot);
InfiniteScroller.game.state.add('Preload', InfiniteScroller.Preload);
InfiniteScroller.game.state.add('Title', InfiniteScroller.GameTitle);
InfiniteScroller.game.state.add('PreGame', InfiniteScroller.PreGame);
InfiniteScroller.game.state.add('Authors', InfiniteScroller.Authors);
InfiniteScroller.game.state.add('Game', InfiniteScroller.Game);
InfiniteScroller.game.state.add('GameOver', InfiniteScroller.GameOver);

InfiniteScroller.game.state.start('Boot');
